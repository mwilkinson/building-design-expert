<?php get_header(); ?>

<div class="row">
	<div class="large-8 columns">
						<?php


						$args = array(
								'posts_per_page'   => 8,
								'offset'           => 0,
								'category'         => '',
								'orderby'          => 'post_date',
								'order'            => 'DESC',
								'include'          => '',
								'exclude'          => '',
								'meta_key'         => '',
								'meta_value'       => '',
								'post_type'        => 'post',
								'post_mime_type'   => '',
								'post_parent'      => '',
								'post_status'      => 'publish',
								'suppress_filters' => true );

						// The Query
						$the_query = new WP_Query( $args );

						// The Loop
						if ( $the_query->have_posts() ) {
							

							while ( $the_query->have_posts() ) {
								
								$the_query->the_post();

								?>

								<div class="post post-line">
					              <div class ="post-title">
					                <h1><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></h1>
					              </div>
					              <div class="row collapse">
					                <div class ="small-12 columns post-info">
					                    <span class="icon-author"><img src="<?php echo bloginfo(template_url); ?>/library/img/author.svg" class="icon_post" /> <?php echo get_the_author(); ?></span>
					                  	<span class="icon-post"><img src="<?php echo bloginfo(template_url); ?>/library/img/clock.svg" class="icon_post" /> <?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></span>
					                </div>
					                <div class ="small-4 columns">

					                </div>
					              </div>
					              <div class ="post-content">
					                <p><?php echo get_the_excerpt(); ?></p>
					                <a href="<?php echo get_permalink(); ?>"> Read Full Post </a>
					              </div>
					            </div>

							<?php
							}
							?>

						<?php

						} else {
							// no posts found
						}
						/* Restore original Post Data */
						wp_reset_postdata();

						?>
					</div>
					<div class="large-4 columns">
						<?php get_sidebar('blogpage'); ?>
					</div>

				</div>

<?php get_footer(); ?>

<?php get_header(); ?>

<div class="row collapse">

        <div class="small-12 medium-8 columns">
          <div class ="small-12 columns">

            <?php 
            
            // POST OF THE WEEK

            $args = array(
                'posts_per_page'   => 1,
                'offset'           => 0,
                'category_name'    => 'featured',
                'orderby'          => 'post_date',
                'order'            => 'DESC',
                'include'          => '',
                'exclude'          => '',
                'meta_key'         => '',
                'meta_value'       => '',
                'post_type'        => 'videos',
                'post_mime_type'   => '',
                'post_parent'      => '',
                'post_status'      => 'publish',
                'suppress_filters' => true );

            // The Query
            $the_query = new WP_Query( $args );

            // The Loop
            if ( $the_query->have_posts() ) {
              

              while ( $the_query->have_posts() ) {
                
                $the_query->the_post();

                $video_type = get_field( "video_type", get_the_ID() );
                $video_url = get_field( "video_url", get_the_ID() );

                switch ($video_type) {
                    case "vimeo":

                        $split = preg_split("/[\/]+/",  $video_url);

                        ?>
                        <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style>
                        <div class='embed-container'>
                          <iframe src="//player.vimeo.com/video/<?php echo $split[2]; ?>?title=0&amp;byline=0&amp;color=6c6e95" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                        </div>
                        <?php
                        break;

                        case "youtube":

                          ?>
                          <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style>
                          <div class='embed-container'>
                            <iframe src='<?php echo $video_url; ?>?showinfo=0&autohide=1' frameborder='0' allowfullscreen></iframe>
                          </div>

                          <?php

                        break;
                }

                ?>

                  <div class="post post-line">
                    <div class ="post-title">
                      <h1><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></h1>
                    </div>
                    <div class="row collapse">
                      <div class ="small-12 columns post-info">
                        <span class="featured">FEATURED VIDEO</span>
                        <span class="icon-post"><img src="<?php echo bloginfo(template_url); ?>/library/img/clock.svg" class="icon_post" /> <?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></span>
                      </div>
                    </div>
                    <div class ="post-content">
                      <p><?php echo get_the_content(); ?></p>
                    </div>
                  </div>

              <?php
              }

            } else {
              // no posts found
            }

            wp_reset_postdata();

            ?>

            

              

            <?php 
            
            // POST OF THE WEEK

            $args = array(
                'posts_per_page'   => 1,
                'offset'           => 0,
                'category_name'    => 'featured',
                'orderby'          => 'post_date',
                'order'            => 'DESC',
                'include'          => '',
                'exclude'          => '',
                'meta_key'         => '',
                'meta_value'       => '',
                'post_type'        => 'post',
                'post_mime_type'   => '',
                'post_parent'      => '',
                'post_status'      => 'publish',
                'suppress_filters' => true );

            // The Query
            $the_query = new WP_Query( $args );

            // The Loop
            if ( $the_query->have_posts() ) {
              

              while ( $the_query->have_posts() ) {
                
                $the_query->the_post();

                ?>

                <div class="post post-line">
                  <div class ="post-title">
                    <h1><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></h1>
                  </div>
                  <div class="row collapse">
                    <div class ="small-12 large-4 columns post-info">
                      <span class="featured">FEATURED POST</span>
                    </div>
                    <div class ="small-12 large-4 columns post-info">
                      <img src="<?php echo bloginfo(template_url); ?>/library/img/author.svg" class="icon_post" /> <?php echo get_the_author(); ?>
                    </div>
                    <div class ="small-12 large-4 columns post-info">
                      <img src="<?php echo bloginfo(template_url); ?>/library/img/clock.svg" class="icon_post" /> <?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?>
                    </div>
                  </div>
                  <div class ="post-content">
                    <p><?php echo get_the_excerpt(); ?></p>
                    <a href="<?php echo get_permalink(); ?>"> Read Full Post </a>
                  </div>
                </div>

              <?php
              }

            } else {
              // no posts found
            }

            wp_reset_postdata();

            ?>

            <?php 
            
            // MOST RECENT

            $args = array(
                'posts_per_page'   => 1,
                'offset'           => 0,
                'orderby'          => 'post_date',
                'order'            => 'DESC',
                'include'          => '',
                'exclude'          => '',
                'meta_key'         => '',
                'meta_value'       => '',
                'post_type'        => 'post',
                'post_mime_type'   => '',
                'post_parent'      => '',
                'post_status'      => 'publish',
                'suppress_filters' => true );

            // The Query
            $the_query = new WP_Query( $args );

            // The Loop
            if ( $the_query->have_posts() ) {
              

              while ( $the_query->have_posts() ) {
                
                $the_query->the_post();

                ?>

                <div class="post">
                  <div class ="post-title">
                    <h1><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></h1>
                  </div>
                  <!--<div class="row collapse">
                    <div class ="small-12 columns post-info">
                      <span class="featured">MOST RECENT</span>
                      <span class="icon-post"><img src="<?php echo bloginfo(template_url); ?>/library/img/author.svg" class="icon_post" /> <?php echo get_the_author(); ?></span>
                      <span class="icon-post"><img src="<?php echo bloginfo(template_url); ?>/library/img/clock.svg" class="icon_post" /> <?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></span>
                    </div>
                  </div>-->
                  <div class="row collapse">
                    <div class ="small-12 large-4 columns post-info">
                      <span class="featured">MOST RECENT</span>
                    </div>
                    <div class ="small-12 large-4 columns post-info">
                      <img src="<?php echo bloginfo(template_url); ?>/library/img/author.svg" class="icon_post" /> <?php echo get_the_author(); ?>
                    </div>
                    <div class ="small-12 large-4 columns post-info">
                      <img src="<?php echo bloginfo(template_url); ?>/library/img/clock.svg" class="icon_post" /> <?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?>
                    </div>
                  </div>
                  <div class ="post-content">
                    <p><?php echo get_the_excerpt(); ?></p>
                    <a href="<?php echo get_permalink(); ?>"> Read Full Post </a>
                  </div>
                </div>

              <?php
              }

            } else {
              // no posts found
            }

            wp_reset_postdata();

            ?>
            
          </div>
        </div>
        <div class="small-4 columns hide-for-small-only">

              <div class="square-holder-row">
                <img src="<?php echo bloginfo(template_url); ?>/library/img/mark1.jpg" class="fp-square fp-s1" />
                <img src="<?php echo bloginfo(template_url); ?>/library/img/square.png" class="fp-square fp-s2" />
              </div>
              <div class="square-holder-row">
                <img src="<?php echo bloginfo(template_url); ?>/library/img/square.png" class="fp-square fp-s3" />
                <img src="<?php echo bloginfo(template_url); ?>/library/img/bde-square.png" class="fp-square fp-s4" />
              </div>

              <div class ="cbox">
                <h5>About this building design expert</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean viverra magna quis odio vestibulum.
                </p>
              </div>

            <?php get_sidebar('home-ad-2'); ?>

            <?php

              function getFeed($feed_url) {
     
                  $content = file_get_contents($feed_url);
                  $x = new SimpleXmlElement($content);
                  $i = 0;
                   
                  foreach($x->channel->item as $entry) {

                      if($i > 4){
                        break;
                      }

                      echo "<li><a href='$entry->link' title='$entry->title'>" . $entry->title . "</a></li>";
                      $i++;
                  }
              }

            ?>

          <div class="rssfeed">
            <ul>
              <li><h4>BDonline <span class="rss-icon"><img src="<?php echo bloginfo(template_url); ?>/library/img/rssfeed.png"></span></h4></li>
              <?php getFeed("http://www.bdonline.co.uk/XmlServers/navsectionRSS.aspx?navsectioncode=965"); ?>
            </ul>
          </div>

          <div class="rssfeed">
            <ul>
              <li>
                <h4>Product News <span class="rss-icon"><img src="<?php echo bloginfo(template_url); ?>/library/img/rssfeed.png"></span></h4>
              </li>
              <?php getFeed("http://www.barbourproductsearch.info/rss/bps-pics-rss.xml"); ?>
            </ul>
          </div>


        </div>

        <div class="row show-for-small-only space-below">

          <div class="small-4 columns">

            <img src="<?php echo bloginfo(template_url); ?>/library/img/mark1.jpg" class="fp-small" />

          </div>
          <div class="small-8 columns">

            <h5>About this building design expert</h5>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean viverra magna quis odio vestibulum.</p>

          </div>

        </div>

<?php get_footer(); ?>

			</div>
	    </div>        
    </div>
  </div>
</div>

	<div class="fullwidth footer" id="footer">
    <div class="row">
      <div class= "large-12 columns">
        <div class="footer">

          <div class="row collapse">
            <div class="small-12 medium-8 columns">
              &copy; Copyright 2014 Building Design Expert
            </div>
            <div class="small-12 medium-2 columns">

              <ul class="footer-list">
                <?php echo get_menu_parts('footer-1'); ?>
              </ul>

            </div>
            <div class="small-12 medium-2 columns">

              <ul class="footer-list">
                <?php echo get_menu_parts('footer-2'); ?>
              </ul>

            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="<?php echo bloginfo(template_url); ?>/library/js/transit.js"></script>
    <script src="<?php echo bloginfo(template_url); ?>/library/js/foundation.min.js"></script>
    <script src="<?php echo bloginfo(template_url); ?>/library/js/foundation/foundation.topbar.js"></script>
    <script src="<?php echo bloginfo(template_url); ?>/library/js/foundation/foundation.reveal.js"></script>
    <script src="<?php echo bloginfo(template_url); ?>/library/js/bde.js"></script>
  </body>
</html>

<?php wp_footer(); ?>

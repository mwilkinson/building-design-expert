<?php

// Flush rewrite rules for custom post types
add_action( 'after_switch_theme', 'bones_flush_rewrite_rules' );

// let's create the function for the custom type
function downloads() { 
	// creating (registering) the custom type 
	register_post_type( 'downloads', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Downloads', 'bonestheme' ), /* This is the Title of the Group */
			'singular_name' => __( 'Download', 'bonestheme' ), /* This is the individual type */
			'all_items' => __( 'All Downloads', 'bonestheme' ), /* the all items menu item */
			'add_new' => __( 'Add Download', 'bonestheme' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Download', 'bonestheme' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Download', 'bonestheme' ), /* Edit Display Title */
			'new_item' => __( 'New Download', 'bonestheme' ), /* New Display Title */
			'view_item' => __( 'View Download', 'bonestheme' ), /* View Display Title */
			'search_items' => __( 'Search Downloads', 'bonestheme' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'No downloads found.', 'bonestheme' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Building design expert free downloads', 'bonestheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-media-archive', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'downloads', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'downloads', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'revisions', 'sticky')
		) /* end of options */
	); /* end of register post type */
	
	/* this adds your post categories to your custom post type */
	register_taxonomy_for_object_type( 'category', 'downloads' );
	/* this adds your post tags to your custom post type */
	register_taxonomy_for_object_type( 'post_tag', 'downloads' );
	
}

	// adding the function to the Wordpress init
	add_action( 'init', 'downloads');

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_downloads',
		'title' => 'Downloads',
		'fields' => array (
			array (
				'key' => 'field_53e38c3c0db53',
				'label' => 'Download',
				'name' => 'download',
				'type' => 'file',
				'instructions' => 'Select or upload your download file',
				'save_format' => 'object',
				'library' => 'all',
			),
			array (
				'key' => 'field_53e38c870db54',
				'label' => 'External Download',
				'name' => 'external_download',
				'type' => 'text',
				'instructions' => 'If you don\'t host the file place the file link here.',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'downloads',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'acf_after_title',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}


	

?>

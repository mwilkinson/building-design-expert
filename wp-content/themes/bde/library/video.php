<?php
/* Bones Custom Post Type Example
This page walks you through creating 
a custom post type and taxonomies. You
can edit this one or copy the following code 
to create another one. 

I put this in a separate file so as to 
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

Developed by: Eddie Machado
URL: http://themble.com/bones/
*/

// Flush rewrite rules for custom post types
add_action( 'after_switch_theme', 'bones_flush_rewrite_rules' );

// Flush your rewrite rules
function bones_flush_rewrite_rules() {
	flush_rewrite_rules();
}

// let's create the function for the custom type
function videos() { 
	// creating (registering) the custom type 
	register_post_type( 'videos', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Videos', 'bonestheme' ), /* This is the Title of the Group */
			'singular_name' => __( 'Video', 'bonestheme' ), /* This is the individual type */
			'all_items' => __( 'All Videos', 'bonestheme' ), /* the all items menu item */
			'add_new' => __( 'Add Video', 'bonestheme' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Video', 'bonestheme' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Video', 'bonestheme' ), /* Edit Display Title */
			'new_item' => __( 'New Video', 'bonestheme' ), /* New Display Title */
			'view_item' => __( 'View Video', 'bonestheme' ), /* View Display Title */
			'search_items' => __( 'Search Videos', 'bonestheme' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'No videos found.', 'bonestheme' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Building design videos', 'bonestheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-video-alt3', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'videos', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'videos', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'revisions', 'sticky')
		) /* end of options */
	); /* end of register post type */
	
	/* this adds your post categories to your custom post type */
	register_taxonomy_for_object_type( 'category', 'videos' );
	/* this adds your post tags to your custom post type */
	register_taxonomy_for_object_type( 'post_tag', 'videos' );
	
}

	// adding the function to the Wordpress init
	add_action( 'init', 'videos');

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_videos',
		'title' => 'Videos',
		'fields' => array (
			array (
				'key' => 'field_53d3a85f75c18',
				'label' => 'Video URL',
				'name' => 'video_url',
				'type' => 'text',
				'instructions' => 'Copy and paste the Youtube or Vimeo URL into this box.',
				'required' => 1,
				'default_value' => '',
				'placeholder' => 'Video URL',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_53d3a8a275c19',
				'label' => 'Video Type',
				'name' => 'video_type',
				'type' => 'select',
				'required' => 1,
				'choices' => array (
					'youtube' => 'YouTube',
					'vimeo' => 'Vimeo',
				),
				'default_value' => '',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_53d3abeadce20',
				'label' => 'Feature on homepage',
				'name' => 'feature_homepage',
				'type' => 'true_false',
				'instructions' => 'The homepage will display the most recent video with this checkbox checked ',
				'message' => '',
				'default_value' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'videos',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'acf_after_title',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}
	

?>

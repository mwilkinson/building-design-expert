<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php wp_title(''); ?></title>

    <link href='http://fonts.googleapis.com/css?family=Oxygen:400,700,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="<?php echo bloginfo(template_url); ?>/library/css/normalize.css" />
    <link rel="stylesheet" href="<?php echo bloginfo(template_url); ?>/library/css/main.css" />

    <link rel="shortcut icon" href="<?php echo bloginfo(template_url); ?>/library/img/bdefav.ico">

    <?php wp_head(); ?>

    <script src="<?php echo bloginfo(template_url); ?>/library/js/vendor/modernizr.js"></script>
  </head>
  <body>
    <div id="wrap">
      <div id="main">
    <div class="fullwidth blue-bar">
      <div class="row">
        <div class="large-6 columns">
          <div class="main-logo">
            <a href="<?php bloginfo('wpurl'); ?>"><img src="<?php echo bloginfo(template_url); ?>/library/img/bde-banner.png" /></a>
          </div>
        </div>
        <div class="large-6 columns">
          <div class="sm-icons hide-for-small-only">
              <a href="http://twitter.com/BD_Expert" target="_blank"><img src="<?php echo bloginfo(template_url); ?>/library/img/twitter.png" height="22" width="22"/></a>
              <a href="http://www.facebook.com/pages/Design-Office/100494056693158"><img src="<?php echo bloginfo(template_url); ?>/library/img/facebook.png" height="20" width="20"/></a>
              <a href="http://www.linkedin.com/profile/view?id=22525610&locale=en_US&trk=tab_pro"><img src="<?php echo bloginfo(template_url); ?>/library/img/linkedin.png" height="20" width="20"/></a>
              <a href="<?php bloginfo('wpurl'); ?>?feed=rss2"><img src="<?php echo bloginfo(template_url); ?>/library/img/rss.png" height="20" width="20"/></a>
          </div>
        </div>
      </div>
    </div>
    <div class="contain-to-grid main-nav"> 
      <nav class="top-bar" data-topbar>

        <ul class="title-area">
          <li class="name"></li>
          <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
        </ul>

        <section class="top-bar-section">
          <ul class="right hide-for-small-only">
            <li class="divider"></li>
            <li class="has-form search-form">
              <div class="clearfix">
                <form method="get" id="searchform" action="<?php bloginfo('home'); ?>/">
                  <input type="text" size="18" value="<?php echo wp_specialchars($s, 1); ?>" name="s" id="s" placeholder="Search" />
                </form>
              </div>
            </li>
          </ul>

          <?php

            $menu_name = 'header-menu';

            function get_menu_parts($menu_name){

              if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {
                
                $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );

                $menu_items = wp_get_nav_menu_items($menu->term_id);

                foreach ( (array) $menu_items as $key => $menu_item ) {
                    $title = $menu_item->title;
                    $url = $menu_item->url;
                    $menu_list .= '<li><a href="' . $url . '">' . $title . '</a></li>';
                }


              }

              return $menu_list;

            }

          ?>

          <ul class="left main-menu">
            <li><a href="<?php bloginfo('wpurl'); ?>/blog">Blog</a></li>
            <li><a href="<?php bloginfo('wpurl'); ?>/videos">Videos</a></li>
            <li><a href="<?php bloginfo('wpurl'); ?>/downloads">Free Downloads</a></li>

            <?php echo get_menu_parts('header-menu'); ?>

            <li><a href="#" id="menu">Menu</a></li>
          </ul>

        </section>
      </nav>
    </div>

    <div class="fullwidth more-box" id="more-box">

      <div class="row">

        <div class="large-3 columns">

          <h3>More</h3>

        </div>

        <div class="large-3 columns">

          <ul>
            <?php echo get_menu_parts('more-col-1'); ?>
          </ul>

        </div>

        <div class="large-3 columns">
          <nav>
            <ul>
              <?php echo get_menu_parts('more-col-2'); ?>
            </ul>
          </nav>

        </div>

        <div class="large-3 columns">

          <ul>
            <?php echo get_menu_parts('more-col-3'); ?>
          </ul>

        </div>

      </div>


    </div>

    <div class="container">  
	    <div class="row collapse">
	        <div class="large-12 columns">


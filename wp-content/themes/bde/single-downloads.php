<?php get_header(); ?>

	<div class="row">

		<div class="large-8 columns">

			<div id="content">

				<div id="inner-content" class="wrap cf">

					<div id="main" class="m-all t-2of3 d-5of7 cf" role="main">

						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<?php

								if(get_field('download')){
										$download = get_field('download');
										$download = $download["url"];

								}else if(get_field('external_download')){
									$download = get_field('external_download');
								}else{
									$download = bloginfo('wpurl')."/404";
								}

							?>

							<div class="post">
				              <div class ="post-title">
				                <h1><?php the_title(); ?></h1>
				              </div>
				              <div class="row collapse">
				              	<div class="small-12 columns">
					              	<ul class="inline-list">
									  <li><img src="<?php echo bloginfo(template_url); ?>/library/img/author.svg" class="icon_post" /></li>
									  <li><?php echo get_the_author(); ?></li>
									  <li><img src="<?php echo bloginfo(template_url); ?>/library/img/clock.svg" class="icon_post" /></li>
									  <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
									</ul>
								</div>
				              </div>

				              <a href="<?php echo $download; ?>" target="_blank">
				              <div class="post-download">
				              	Download
				              </div>
				          	  </a>
				              <div class ="post-content">
				                <p><?php the_content(); ?></p>
				              </div>
				            </div>

				            <div id="disqus_thread"></div>
						    <script type="text/javascript">
						        var disqus_shortname = 'buildingdesignexpert';
						        var disqus_title = "<?php the_title(); ?>";

						        (function() {
						            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
						            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
						            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
						        })();
						    </script>
						    <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>

						<?php endwhile; ?>

						<?php else : ?>

							<article id="post-not-found" class="hentry cf">
									<header class="article-header">
										<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
									</header>
									<section class="entry-content">
										<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
									</section>
									<footer class="article-footer">
											<p><?php _e( 'This is the error message in the single.php template.', 'bonestheme' ); ?></p>
									</footer>
							</article>

						<?php endif; ?>

					</div>

					

				</div>

			</div>

		</div>
		<div class="large-4 columns">

			<?php get_sidebar('blogpage'); ?>

		</div>

	</div>

<?php get_footer(); ?>
